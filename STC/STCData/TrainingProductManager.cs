﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace STCData
{
    public class TrainingProductManager
    {

        public List<TrainingProduct>Get()
        {
            return Get(new TrainingProduct());
        }

        public List<TrainingProduct>Get(TrainingProduct entity)
        {

            List<TrainingProduct> ret = new List<TrainingProduct>();

            ret = CreateMockData();

            // Do any searching
            if (!string.IsNullOrEmpty(entity.ProductName))
            {
                ret = ret.FindAll(
                  p => p.ProductName.ToLower().
                        StartsWith(entity.ProductName,
                                  StringComparison.CurrentCultureIgnoreCase));
            }


            return ret;
        }
        private List<TrainingProduct> CreateMockData()
        {
            List<TrainingProduct> ret = new List<TrainingProduct>();

            ret.Add(new TrainingProduct()
            {
                ProductId = 1,
                ProductName = "C#",
                IntroductionDate = Convert.ToDateTime("6/11/2015"),
                Url = "https://bit.ly/lSnzcoi",
                Price = Convert.ToDecimal(29.00)
            });

            ret.Add(new TrainingProduct()
            {
                ProductId = 2,
                ProductName = "Java",
                IntroductionDate = Convert.ToDateTime("3/1/2017"),
                Url = "https://bit.ly/lSnzcoi",
                Price = Convert.ToDecimal(29.00)
            });

            ret.Add(new TrainingProduct()
            {
                ProductId = 3,
                ProductName = "C++",
                IntroductionDate = Convert.ToDateTime("3/6/2017"),
                Url = "https://bit.ly/lSnzcoi",
                Price = Convert.ToDecimal(60.00)
            });
            return ret;
        }
    }
}
